About
=====

DashFoo is a game-oriented command handling library designed to handle commands in a very abstract sense.

DashFoo lets you handle prefixed and non-prefixed commands, custom formats, and standard formats. It supports typed transforms from the input end (plaintext) to the business end (typed objects).

DashFoo is, for the most part, fully pluggable. It allows for the parser, dispatcher, and other such things to be fully re-written to meet your specific needs.

DashFoo is developed using IntelliJ Idea (CE 13).

License
=======

DashFoo is licensed under the [Apache 2.0 Software License](https://www.apache.org/licenses/LICENSE-2.0)

Get DashFoo
===========

DashFoo is available from [Maven Central](http://search.maven.org/#artifactdetails|pw.hysteria.input|dashfoo|1.5|jar).

Building DashFoo
================

DashFoo may be built using the included build script.

Website
=======

DashFoo is a Hysteria project, and has a [project page](http://hysteria.pw/projects/dashfoo)