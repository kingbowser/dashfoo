/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.test.command;

import pw.hysteria.input.dashfoo.command.FlagDispatcher;
import pw.hysteria.input.dashfoo.command.IsFlag;
import pw.hysteria.input.dashfoo.command.MethodAsInvokableFactory;
import pw.hysteria.input.dashfoo.command.impl.FlaggedCommand;
import pw.hysteria.input.dashfoo.command.parsing.UserInputFlagParser;
import pw.hysteria.input.dashfoo.command.parsing.impl.RegexSmartParser;
import pw.hysteria.input.dashfoo.command.parsing.impl.SimpleRegexProvider;
import pw.hysteria.input.dashfoo.command.parsing.impl.SimpleSanityChecker;

import java.util.ArrayList;

/**
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 2:34 PM
 * Refer to LICENSE for licensing information
 */
public class TestCommand extends FlaggedCommand {


    private UserInputFlagParser parser;
    private final MethodAsInvokableFactory methodiser = new MethodAsInvokableFactory();

    public TestCommand() {
        super(new FlagDispatcher());
    }


    @IsFlag(handles = {"empty_flag"})
    @SuppressWarnings("unused")
    public void nullary() {
        System.out.println("NULLARY");
    }

    @IsFlag(handles = {"flag_with_arguments"})
    public void flagWithArguments(String stringArgument, int numberArgument) {
        System.out.println("Did it work: " + stringArgument + " " + numberArgument + "?");
    }

    @IsFlag(handles = "@default")
    public void rootFlags(String stringArg, int numArg) {
        System.out.println("Call to default flag: " + stringArg + " " + numArg);
    }

    @Override
    public String[] getHandles() {
        return new String[]{"t", "test", "tst", "c", "command"};
    }


    @Override
    protected void initCommand() {
        parser = new RegexSmartParser(new SimpleRegexProvider(new TestCommandSpec()), new SimpleSanityChecker(getFlagDispatcher()));

        ArrayList<MethodAsInvokableFactory.MethodAsFlag> foundFlags = methodiser.constructFromClass(this);
        System.out.println(foundFlags);

        getFlagDispatcher().addAll(foundFlags);
    }

    @Override
    protected final UserInputFlagParser getFlagParser() {
        return parser;
    }
}
