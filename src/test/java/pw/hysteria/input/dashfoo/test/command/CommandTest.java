/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.test.command;


import org.junit.Test;
import pw.hysteria.input.dashfoo.api.Initializes;
import pw.hysteria.input.dashfoo.command.Command;
import pw.hysteria.input.dashfoo.command.CommandDispatcher;
import pw.hysteria.input.dashfoo.command.parsing.impl.SimpleRegexProvider;

/**
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 2:20 PM
 * Refer to LICENSE for licensing information
 */
public class CommandTest {

    @Test
    public void runTest() throws Command.CommandException, Initializes.InitializationException {

        Command test = new TestCommand();

        CommandDispatcher dispatcher = new CommandDispatcher(new SimpleRegexProvider(new TestCommandSpec()));
        dispatcher.registerCommand(test);


//        dispatcher.dispatchCommand("command string_root_argument 1234567890 /flag_with_arguments string 1234567890 /empty_flag");

    }

}
