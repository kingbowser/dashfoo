

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing;

/**
 * Defines a contract for high-level flag existance and arity checking
 * Provides a map of invokable names to invokable data for parser reference
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 11:30 PM
 * Refer to LICENSE for licensing information
 */
public interface CommandSanityChecker {

    /**
     * Determine whether or not the provided flag is registered or provided by the implementation
     *
     * @param flag flag to validity
     * @return validity
     */
    public boolean flagExists(String flag);

    /**
     * Determine whether or not the specified flag is sane
     *
     * @param flag     flag
     * @param argCount actual arity
     * @return sanity
     */
    public boolean flagIsSane(String flag, int argCount);

    /**
     * Get the arity of the specified flag
     *
     * @param flag flag
     * @return flag arity
     */
    public int getArity(String flag);

}
