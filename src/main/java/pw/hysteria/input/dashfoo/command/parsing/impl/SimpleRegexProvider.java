/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing.impl;

import pw.hysteria.input.dashfoo.command.parsing.CommandFormattingProvider;
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 3:42 PM
 * Refer to LICENSE for licensing information
 */
public class SimpleRegexProvider implements CommandRegexProvider {

    private final CommandFormattingProvider configuration;

    private Pattern explosion;

    private Pattern flag;

    private Pattern handle;

    public SimpleRegexProvider(CommandFormattingProvider configuration) {
        this.configuration = configuration;

        recompile();
    }

    public void recompile() {

         /*
            <DEPRECATED> (?<=$IG_BEG) .+ (?=$IG_END) | (?<!$IG_BEG) [^$IG_BEG$EG_END$EXPLODE]+ (?!$IG_END)
            (?<!\\)(".*?)(?<!\\)"|([^|\s]+)
            Long-ass regex warning
         */
        explosion =
                Pattern.compile("(?<!" + configuration.getEscapeChar() + "{1})(" + configuration.getIgnoreChar() + ".*?)(?<!" + configuration.getEscapeChar() + "{1})\"|([^" + configuration.getExplodeChar() + "]+)");

//        System.out.println("Explosion pattern: " + explosion.toString());

        /*
            (?<!$ESCAPE)$FLAG[^$EXPLODE]+(?=$EXPLODE)
         */
        flag =
                Pattern.compile("(?<!" + configuration.getEscapeChar() + "{1})" + configuration.getFlagChar() + "([^" + configuration.getExplodeChar() + "]+)");

//        System.out.println("Flag pattern: " + flag.toString());

        /*
            ((?<=^-)[^\s]+)
            The handle is the first element - that's why this works
            This means we can capture the first element and then greedy the rest. This way Matcher is happy, and so it .matches()
         */
        handle =
                Pattern.compile("^" + configuration.getHandleChar() + "(\\w+)(.+)?");
//                Pattern.compile("^(.+)(.+)?");

        /*
            From a logical standpoint, validCommand should just be the handle with a greedy include
         */
//        validCommand = Pattern.compile("^" + configuration.getHandleChar() + "([\\w\\S]+).+");

//        System.out.println("Handle pattern:" + handle.toString());

    }


    @Override
    public Pattern getExplosionPattern() {
        return explosion;
    }

    @Override
    public Pattern getFlagPattern() {
        return flag;
    }

    @Override
    public Pattern getHandlePattern() {
        return handle;
    }

    @Override
    public String postProcessString(String post) {
        post = post.replaceAll("(?<!" + configuration.getEscapeChar() + ")" + configuration.getEscapeChar(), "");
        post = post.replaceAll("(?<!" + configuration.getEscapeChar() + ")" + configuration.getIgnoreChar(), "");
        return post;
    }

    @Override
    public boolean isCommand(String command) {

        return command.matches(getHandlePattern().pattern());

    }

    @Override
    public String getHandle(String command) {

        Matcher grepHandle = getHandlePattern().matcher(command);
        if (grepHandle.find())
            return grepHandle.group(1);
        else
            return null;

    }

    @Override
    public boolean isFlag(String flag) {
        return flag.matches(getFlagPattern().pattern());
    }

    @Override
    public String getFlag(String flag) {

        Matcher grepHandle = getFlagPattern().matcher(flag);
        if (grepHandle.find())
            return grepHandle.group(1);
        else
            return null;

    }

}
