/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * The command-flag invokable dispatcher
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 10:28 PM
 * Refer to LICENSE for licensing information
 */
public class FlagDispatcher implements HasFlags {

    // Variables

    private final Map<String, Flag> invokableMap = new HashMap<>();

    // Methods

    /**
     * Register an flag (a flag)
     *
     * @param flag flag
     */
    public void registerInvokable(Flag flag) {

        /*
            Multi-handle sanity check
         */
        for (String handle : flag.getHandles()) {
            if (getFlags().containsKey(handle))
                throw new IllegalArgumentException("Supplied flag has handles that overlap with another flag");
        }

        for (String handle : flag.getHandles()) {
            getFlags().put(handle, flag);
        }

    }

    /**
     * UnRegister an flag (a flag)
     *
     * @param flag flag
     */
    public void unRegisterInvokable(Flag flag) {

        /*
            Sanity check to insure that an unregistered flag can't unregister another flag
         */
        if (!getFlags().values().contains(flag))
            throw new IllegalArgumentException("Supplied flag is not registered.");

        for (String handle : flag.getHandles())
            getFlags().remove(handle);

    }

    /**
     * Add all flags in a Collection[Flag]
     *
     * @param addFrom collection
     */
    public void addAll(Collection<? extends Flag> addFrom) {

        for (Flag flag : addFrom) {
            registerInvokable(flag);
        }

    }

    public void dispatch(Map<String, String[]> flagmap) throws Flag.FlagException {

        for (Map.Entry<String, String[]> entry : flagmap.entrySet()) {

            if (getFlags().containsKey(entry.getKey())) {

                getFlags().get(entry.getKey()).invokeFlag(entry.getValue());

            } else
                throw new IllegalArgumentException("Non-existant flag passed to dispatcher");

        }

    }

    /*
        HasFlags
     */

    @Override
    public Map<String, Flag> getFlags() {
        return invokableMap;
    }


}
