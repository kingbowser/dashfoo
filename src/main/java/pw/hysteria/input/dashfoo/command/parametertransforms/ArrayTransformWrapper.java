/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parametertransforms;

import pw.hysteria.input.dashfoo.command.ParamTypeTransform;

import java.util.ArrayList;

/**
 * Transforms a comma-separated list into an ArrayOfType* using the transform for each of those
 * This shouldn't be used in conjunction with high-load transformers in the same thread as the rest of the application
 * for obvious reasons (I supposed handling args from the entry method is an exception)
 *
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 7:58 PM
 * Refer to LICENSE for licensing information
 */
public class ArrayTransformWrapper<T> implements ParamTypeTransform<T[]> {

    /**
     * ParamTypeTransform tasked with handling each element
     */
    private final ParamTypeTransform<T> elementTransformer;

    /**
     * Plaintext element delimiter
     */
    private final char delimiter;

    /**
     * Construct the ArrayTransformWrapper with a specified delimiter
     *
     * @param elementTransformer regular transformer
     * @param delimiter          element delimiter used to parse the plaintext
     */
    public ArrayTransformWrapper(ParamTypeTransform<T> elementTransformer, char delimiter) {

        this.elementTransformer = elementTransformer;
        this.delimiter = delimiter;

    }

    /**
     * Construct the ArrayTransformWrapper with the default delimiter
     *
     * @param elementTransformer regular transformer used to transform each element
     */
    public ArrayTransformWrapper(ParamTypeTransform<T> elementTransformer) {
        this(elementTransformer, ',');
    }

    @Override
    @SuppressWarnings("unchecked") // Performed an implicitly typesafe cast.
    public T[] transform(String string) {

        /*
            Explode the string from the delimiter
         */
        final String[] exploded = string.split(String.valueOf(delimiter));
        final ArrayList<T> transferMedium = new ArrayList<>();

        /*
            Transform each element and then add it to the transfer medium
         */
        for (String transformFrom : exploded)
            transferMedium.add(elementTransformer.transform(transformFrom));

        /*
            Convert the transfer medium to an array, and then turn that array into an ArrayOfType<T>
         */
        return (T[]) transferMedium.toArray();
    }
}
