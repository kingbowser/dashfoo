

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing;

import java.util.regex.Pattern;

/**
 * Provides regex patterns and functions for parseing user command input
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 3:40 PM
 * Refer to LICENSE for licensing information
 */
public interface CommandRegexProvider {

    /**
     * Get the pattern used to explode user input into various elements for further parsing
     *
     * @return pattern
     */
    public Pattern getExplosionPattern();

    /**
     * Get the pattern used to validate flags and strip flag sequences
     *
     * @return pattern
     */
    public Pattern getFlagPattern();

    /**
     * Get the pattern used to validate commands (via checking for a handle at the beginning) and to strip handle sequences
     *
     * @return pattern
     */
    public Pattern getHandlePattern();

    /**
     * Perform any other processing on elements that must be performed outside of the regex engine
     *
     * @param post element that is in need of processing
     * @return element
     */
    public String postProcessString(String post);

    /**
     * Determine if a string is a valid command
     *
     * @param command possible command
     * @return command validity
     */
    public boolean isCommand(String command);

    /**
     * Get the handle of a command
     *
     * @param command command
     * @return handle
     */
    public String getHandle(String command);

    /**
     * Determine if an element is a flag, or conversely a parameter
     *
     * @param flag possible flag
     * @return flag validity
     */
    public boolean isFlag(String flag);

    /**
     * Get the name of a valid flag
     *
     * @param flag flag
     * @return name
     */
    public String getFlag(String flag);

}
