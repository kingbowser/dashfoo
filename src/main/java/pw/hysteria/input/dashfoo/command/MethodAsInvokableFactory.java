

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

import pw.hysteria.input.dashfoo.command.parametertransforms.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 7:39 PM
 * Refer to LICENSE for licensing information
 */
public final class MethodAsInvokableFactory {

    /**
     * Defines the appropriate parameter transforms to assign to each method parameter
     */
    private final Map<Class<?>, ParamTypeTransform<?>> transformMap = new HashMap<>();

    public MethodAsInvokableFactory() {

        /*
            Register default transforms (array and single)
         */

        registerTransform(String.class, new StringTransform());
        registerTransform(Boolean.class, new BooleanTransform());

        registerTransform(Byte.class, new ByteTransform());
        registerTransform(Short.class, new ShortTransform());
        registerTransform(Integer.class, new IntegerTransform());
        registerTransform(Long.class, new LongTransform());
        registerTransform(Float.class, new FloatTransform());
        registerTransform(Double.class, new DoubleTransform());

        registerTransform(boolean.class, new BooleanTransform());

        registerTransform(byte.class, new ByteTransform());
        registerTransform(short.class, new ShortTransform());
        registerTransform(int.class, new IntegerTransform());
        registerTransform(long.class, new LongTransform());
        registerTransform(float.class, new FloatTransform());
        registerTransform(double.class, new DoubleTransform());

        /*
            The generics API prevents us from transforming primitive arrays without mucking up our code.
         */

        registerTransform(String[].class, new ArrayTransformWrapper<>(new StringTransform()));
        registerTransform(Boolean[].class, new ArrayTransformWrapper<>(new BooleanTransform()));

        registerTransform(Byte[].class, new ArrayTransformWrapper<>(new ByteTransform()));
        registerTransform(Short[].class, new ArrayTransformWrapper<>(new ShortTransform()));
        registerTransform(Integer[].class, new ArrayTransformWrapper<>(new IntegerTransform()));
        registerTransform(Long[].class, new ArrayTransformWrapper<>(new LongTransform()));
        registerTransform(Float[].class, new ArrayTransformWrapper<>(new FloatTransform()));
        registerTransform(Double[].class, new ArrayTransformWrapper<>(new DoubleTransform()));

    }

    /**
     * Register a transform for a class
     *
     * @param ptClass   class of parameter
     * @param ptFactory factory to produce instance of parameter type
     * @param <PT>      type of parameter
     */
    public final <PT> void registerTransform(Class<PT> ptClass, ParamTypeTransform<PT> ptFactory) {

        if (transformMap.containsKey(ptClass))
            throw new IllegalArgumentException(ptClass.getName() + " already has a transform registered. Please unregister it first.");

        transformMap.put(ptClass, ptFactory);

    }

    /**
     * Remove a mapping for a ParameterTypeTransform
     *
     * @param ptClass class of the parameter type
     */
    @SuppressWarnings("unused")
    public void unregisterTransform(Class<?> ptClass) {

        if (!transformMap.containsKey(ptClass))
            throw new IllegalArgumentException(ptClass.getName() + " is not registered to a transform. Please register it first.");

        transformMap.remove(ptClass);

    }

    @SuppressWarnings("unchecked")
    public <PT> ParamTypeTransform<PT> translateParamToTransform(Class<PT> paramClass) {

        if (!transformMap.containsKey(paramClass))
            throw new IllegalArgumentException("No such parameter mapping exists");

        return (ParamTypeTransform<PT>) transformMap.get(paramClass);
    }

    /**
     * Construct a MethodAsFlag from any given method
     *
     * @param handles     handles
     * @param method      method to be invoked
     * @param methodOwner instance of the class that owns the method
     * @return compiled MethodAsFlag
     */
    private MethodAsFlag constructFromMethod(String[] handles, Method method, Object methodOwner) {

        if (methodOwner == null)
            throw new IllegalArgumentException("Method owner is null");

        final ArrayList<ParamTypeTransform<?>> transforms = new ArrayList<>();

        for (Class<?> paramType : method.getParameterTypes()) {
            try {
                transforms.add(translateParamToTransform(paramType));
            } catch (IllegalArgumentException exception) {
                throw new IllegalStateException("Provided method contains unrecognized parameter types");
            }
        }

        return new MethodAsFlag(handles, transforms, method, methodOwner);

    }

    /**
     * Construct a MethodAsFlag from an @IsFlag annotated method
     *
     * @param method      annotated method
     * @param methodOwner container class instance
     * @return compiled MethodAsFlag
     */
    public MethodAsFlag constructFromMethod(Method method, Object methodOwner) {

        if (!method.isAnnotationPresent(IsFlag.class))
            throw new IllegalArgumentException("Provided method is not annotated with @IsFlag");

        return constructFromMethod(method.getAnnotation(IsFlag.class).handles(), method, methodOwner);

    }

    /**
     * Construct an ArrayList of MethodsAsInvokable from an instance of a class
     *
     * @param methodsOwner class instance
     * @return enumerated methods
     */
    public ArrayList<MethodAsFlag> constructFromClass(Object methodsOwner) {

        final ArrayList<MethodAsFlag> compiledMethods = new ArrayList<>();

        /*
            Iterate through methods and spot the methods with @IsFlag
         */
        for (Method method : methodsOwner.getClass().getMethods()) {
            if (method.isAnnotationPresent(IsFlag.class)) {
                compiledMethods.add(constructFromMethod(method, methodsOwner));
            }
        }

        return compiledMethods;
    }


    /**
     * Defines an invokable that has been constructed based on a method signature
     *
     * WARNING: MESSY CODE ZONE
     *
     * Creator: KingBowser
     * Date: 5/26/13
     * Time: 7:32 PM
     * Refer to LICENSE for licensing information
     */
    public static final class MethodAsFlag implements Flag, HelpProvider {

        /**
         * Defines the argument types used at each position
         */
        private final List<ParamTypeTransform<?>> paramTypeTransforms;

        /**
         * The method to invoke
         */
        private final Method invokeMethod;

        /**
         * The instance of the method owner to invoke the method against
         */
        private final Object invokeAgainst;

        /**
         * The handle of the invokable
         */
        private final String[] handles;

        /**
         * Default Constructor
         *
         * @param acceptableFlags         array of possible handles
         * @param typeTransforms          list of transforms in the order that they will be needed
         * @param methodToInvoke          method to invokeFlag
         * @param methodContainerInstance an instance of the object containing the invoking method
         */
        MethodAsFlag(String[] acceptableFlags, List<ParamTypeTransform<?>> typeTransforms, Method methodToInvoke,
                     Object methodContainerInstance) {

            this.handles = acceptableFlags;

            this.paramTypeTransforms = typeTransforms;
            this.invokeMethod = methodToInvoke;
            this.invokeAgainst = methodContainerInstance;


        }

        @Override
        public String[] getHandles() {
            return handles;
        }

        @Override
        public void invokeFlag(String... args) throws FlagInvocationException {

            /*
                Are there too many args?
            */
            if (args.length > paramTypeTransforms.size())
                throw new FlagInvocationException("Number of arguments passed exceeds number of expected arguments", this, args);

            /*
                Transform our arguments into the appropriate format
            */
            Object[] methodArgs = new Object[args.length];


            int positionInParams = 0;

            for (String arg : args) {
                methodArgs[positionInParams] = paramTypeTransforms.get(positionInParams).transform(arg);
                positionInParams++;
            }

            /*
                Make sure we can invoke from this scope (JIC)
            */
            invokeMethod.setAccessible(true);

            /*
                Method return trap
            */
            Object methodReturn;

            try {
                methodReturn = invokeMethod.invoke(invokeAgainst, methodArgs);
            } catch (IllegalAccessException e) {
                throw new IllegalStateException("Unable to access invocation target from current scope");
            } catch (InvocationTargetException e) {
                throw new IllegalArgumentException("Passed an invalid method container during construction");
            }

        }

        @Override
        public int getArity() {
            return paramTypeTransforms.size();
        }

        @Override
        public final HelpProvider getHelpProvider() {
            return this;
        }

        @Override
        public final String getDescription() {
            return null;
        }

        @Override
        public final String getHelp() {
            return null;
        }
    }

}
