

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

/**
 * Defines a flag in a command sequence.
 * Creator: KingBowser
 * Date: 5/25/13
 * Time: 10:54 AM
 * Refer to LICENSE for licensing information
 */
public interface Flag extends HasHelpProvider {

    /**
     * Get the handles that denote the flag
     *
     * @return possible handles
     */
    public String[] getHandles();

    /**
     * Get the arity of the flag
     * 0 denotes a nullary function
     *
     * @return arity
     */
    public int getArity();

    /**
     * Invoke the flag with the provided arguments
     *
     * @param args parameters of the flags
     * @throws pw.hysteria.input.dashfoo.command.Flag.FlagException when an error occurs during flag invocation
     */
    public void invokeFlag(String... args) throws FlagException;

    /**
     * Describes an exception that occurs in relation to a flag
     */
    class FlagException extends Exception {

        private final Flag relatesTo;

        protected FlagException(String message, Flag related){
            super(message);
            relatesTo = related;
        }

        protected FlagException(String message){
            this(message, null);
        }

        /**
         * Get the Flag related to the exception
         * @return flag
         */
        public final Flag getFlag(){
            return relatesTo;
        }

        /**
         * Find out if the flag was defined
         * @return flag existence
         */
        public final boolean flagIsKnown(){
            return relatesTo != null;
        }

    }

    /**
     * Describes an exception related to the invocation of a flag
     */
    class FlagInvocationException extends FlagException {

        private final String[] flagParameters;

        protected FlagInvocationException(String message, Flag flag, String[] parameters){
            super(message, flag);
            flagParameters = parameters;
        }

        protected FlagInvocationException(String message, String[] parameters){
            this(message, null, parameters);
        }

        protected FlagInvocationException(String message){
            this(message, null);
        }

        /**
         * Determine whether or not the parameters related to the exception are known
         * @return parameter knowledge
         */
        public boolean parametersAreKnown(){
            return flagParameters != null;
        }

        /**
         * Get the number of parameters passed to the flag at the time of the exception
         * @return parameter count
         */
        public int getParameterCount(){
            return flagParameters.length;
        }

        /**
         * Get the list of parameters passed to the flag
         * @return parameters
         */
        public String[] getParameters(){
            return flagParameters;
        }

    }

}
