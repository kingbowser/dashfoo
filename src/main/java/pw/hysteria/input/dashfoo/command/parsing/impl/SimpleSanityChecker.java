/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing.impl;

import pw.hysteria.input.dashfoo.command.HasFlags;
import pw.hysteria.input.dashfoo.command.parsing.CommandSanityChecker;

/**
 * Accesses a HasFlags in order to perform sanity checks
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 12:08 PM
 * Refer to LICENSE for licensing information
 */
public final class SimpleSanityChecker implements CommandSanityChecker {

    // Variables

    private final HasFlags invokableProvider;

    // Constructors

    public SimpleSanityChecker(HasFlags invokableProvider) {
        this.invokableProvider = invokableProvider;
    }

    // Methods

    @Override
    public boolean flagExists(String flag) {
        return invokableProvider.getFlags().containsKey(flag);
    }

    @Override
    public boolean flagIsSane(String flag, int argCount) {

        if (!flagExists(flag))
            return false;

//        System.out.println("FC: " + flag + "; arity: " + argCount);

        return getArity(flag) == argCount;

    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if no such flag exists
     */
    @Override
    public int getArity(String flag) {

        if (!flagExists(flag))
            throw new IllegalStateException("Cannot get arity for " + flag + " because it does not exist!");

        return invokableProvider.getFlags().get(flag).getArity();
    }

}
