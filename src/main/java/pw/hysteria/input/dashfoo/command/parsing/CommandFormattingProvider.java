

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing;

/**
 * Creator: KingBowser
 * Date: 5/27/13
 * Time: 12:01 AM
 * Refer to LICENSE for licensing information
 */
public interface CommandFormattingProvider {

    /**
     * Get the sequence of characters that delimit an escape
     *
     * @return escape delimitation sequence
     */
    public String getEscapeChar();

    /**
     * Get the sequence of characters that delimit ignore groups $...$
     *
     * @return ignore delimitation sequence
     */
    public String getIgnoreChar();

    /**
     * Get the sequence of characters that delimit elements in the command
     *
     * @return element delimitation sequence
     */
    public String getExplodeChar();

    /**
     * Get the sequence of characters that delimits a flag
     *
     * @return flag delemitation sequence
     */
    public String getFlagChar();

    /**
     * Get the sequence of characters that delimits the command handle
     *
     * @return handle delimitation sequence
     */
    public String getHandleChar();

}
