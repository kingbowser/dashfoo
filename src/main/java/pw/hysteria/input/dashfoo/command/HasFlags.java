

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

import java.util.Map;

/**
 * Provides a contract for a class that has a map of Invokables
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 12:05 PM
 * Refer to LICENSE for licensing information
 */
public interface HasFlags {

    public static final String DEFAULT_FLAG = "@default";

    /**
     * Get the flags belonging to the HasFlags, in a Handle=Flag map
     *
     * @return flags
     */
    public Map<String, Flag> getFlags();

}
