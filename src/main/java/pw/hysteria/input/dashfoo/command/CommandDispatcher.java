

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

import pw.hysteria.input.dashfoo.api.Initializes;
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider;

import java.util.Collection;
import java.util.HashMap;

/**
 * Holds and dispatches commands
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 2:21 PM
 * Refer to LICENSE for licensing information
 */
public class CommandDispatcher {

    // Variables

    private final HashMap<String, Command> loadedCommands = new HashMap<>();

    private final CommandRegexProvider regexProvider;

    // Constructors

    public CommandDispatcher(CommandRegexProvider regexProvider) {
        this.regexProvider = regexProvider;
    }

    // Methods

    /**
     * Register the command to the registry
     *
     * @param command command
     * @throws pw.hysteria.input.dashfoo.api.Initializes.InitializationException when an error occurs while initializing the command
     */
    public void registerCommand(Command command) throws Initializes.InitializationException {

        for (String handle : command.getHandles()) {
            if (loadedCommands.keySet().contains(handle))
                throw new IllegalArgumentException("A supplied command contains overlapping handles");
        }

        command.initialize();

        for (String handle : command.getHandles())
            loadedCommands.put(handle, command);

    }

    /**
     * Unregister a registered command
     *
     * @param command command
     */
    public void unRegisterCommand(Command command) {

        if (!loadedCommands.values().contains(command))
            throw new IllegalArgumentException("No such command is loaded");

        for (String handle : command.getHandles())
            loadedCommands.remove(handle);

    }

    /**
     * Dispatch the command with its entire user input from the command source
     *
     * @param command command text
     * @throws pw.hysteria.input.dashfoo.command.Command.CommandException when an error occurs when invoking the command
     */
    public void dispatchCommand(String command) throws Command.CommandException {

        String handle = regexProvider.getHandle(command);

        if (!loadedCommands.containsKey(handle))
            throw new IllegalArgumentException("No such command (" + handle + ") exists");

        loadedCommands.get(handle).invokeCommand(command);
    }

    public HashMap<String, Command> getLoadedCommands(){ return new HashMap<String, Command>(this.loadedCommands); }

}
