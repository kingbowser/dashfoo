

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command;

import pw.hysteria.input.dashfoo.api.Initializes;

/**
 * Represents a command without implicit arguments
 * Creator: KingBowser
 * Date: 5/27/13
 * Time: 4:25 PM
 * Refer to LICENSE for licensing information
 */
public abstract class Command implements pw.hysteria.input.dashfoo.command.HasHelpProvider, Initializes {

    /**
     * Invoke the command
     *
     * @param commandText entire command text from commandline
     * @throws pw.hysteria.input.dashfoo.command.Command.CommandException when an invocation error occurs.
     */
    public abstract void invokeCommand(String commandText) throws CommandException;

    /**
     * Get the handles for the command
     *
     * @return handle
     */
    public abstract String[] getHandles();

    /**
     * Describes an exception related to a command
     */
    public class CommandException extends Exception {

        private Command relatedCommand;

        public CommandException(String message, Command related){
            super(message);
            relatedCommand = related;
        }

        /**
         * Get the command related to this exception
         * @return command
         */
        public final Command getCommand(){
            return relatedCommand;
        }

        /**
         * Determine whether or not the command is known to the exception
         * @return command knowledge/nullity
         */
        public final boolean commandIsKown(){
            return relatedCommand != null;
        }

    }

    /**
     * Describes an exception related to the execution of a command
     */
    protected class CommandInvocationException extends CommandException {

        private final String commandText;

        protected CommandInvocationException(String message, Command command, String directive){
            super(message, command);
            commandText = directive;
        }

        protected CommandInvocationException(String message, Command command){
            this(message, command, null);
        }

        public CommandInvocationException(String message){
            this(message, null, null);
        }

        /**
         * Determine whether or not the directive of the command is known to the exception
         * @return inverse directive nullity
         */
        public boolean directiveIsKnown(){
            return commandText != null;
        }

        /**
         * Get the directive(s) passed to the command
         * @return directive(s)
         */
        public String getDirective(){
            return commandText;
        }

    }

}
