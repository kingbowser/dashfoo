/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.help;

import pw.hysteria.input.dashfoo.command.HelpProvider;

/**
 * Defines a HelpProvider who's content is provided at the time of construction.
 * Creator: KingBowser
 * Date: 5/27/13
 * Time: 4:33 PM
 * Refer to LICENSE for licensing information
 */
public final class SimpleHelpProvider implements HelpProvider {

    // Variables

    private final String description;

    private final String help;

    // Constructors

    public SimpleHelpProvider(String description, String help) {
        this.description = description;
        this.help = help;
    }

    // Methods

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getHelp() {
        return help;
    }

}
