/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.impl;

import pw.hysteria.input.dashfoo.api.Initializes;
import pw.hysteria.input.dashfoo.command.*;
import pw.hysteria.input.dashfoo.command.parsing.UserInputFlagParser;

import java.util.Map;

/**
 * Creator: KingBowser
 * Date: 5/28/13
 * Time: 1:58 PM
 * Refer to LICENSE for licensing information
 */
public abstract class FlaggedCommand extends Command {

    /**
     * Flag dispatcher does just that
     */
    private final FlagDispatcher flagDispatcher;

    public FlaggedCommand(FlagDispatcher dispatcher) {
        this.flagDispatcher = dispatcher;
    }

    /**
     * Help provider for command
     */
    private HelpProvider helpContract = NullHelpProvider.PROVIDER;

    @Override
    public final void invokeCommand(String commandText) throws CommandException {
        Map<String, String[]> mappedInput = getFlagParser().mapUserInput(commandText);

        try{
            flagDispatcher.dispatch(mappedInput);
        } catch (Flag.FlagException fExcept){
            throw new CommandInvocationException("An exception was caught whilst executing the Flag: " + fExcept.getMessage());
        }
    }

    public final void initialize() throws Initializes.InitializationException {
        initCommand();
    }

    @Override
    public final HelpProvider getHelpProvider() {
        return helpContract;
    }

    protected final FlagDispatcher getFlagDispatcher() {
        return flagDispatcher;
    }

    protected final void setHelpProvider(HelpProvider contract) {
        this.helpContract = contract;
    }

    protected abstract void initCommand() throws InitializationException;

    protected abstract UserInputFlagParser getFlagParser();

}
