/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parametertransforms;

import pw.hysteria.input.dashfoo.command.ParamTypeTransform;

/**
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 7:51 PM
 * Refer to LICENSE for licensing information
 */
public class IntegerTransform implements ParamTypeTransform<Integer> {

    @Override
    public Integer transform(String string) {
        return Integer.parseInt(string);
    }

}
