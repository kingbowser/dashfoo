/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing.impl;

import pw.hysteria.input.dashfoo.command.HasFlags;
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider;
import pw.hysteria.input.dashfoo.command.parsing.CommandSanityChecker;
import pw.hysteria.input.dashfoo.command.parsing.UserInputFlagParser;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;


public class RegexSmartParser implements UserInputFlagParser {

    /**
     * Describes the format by which the entire input is split
     */
    private final CommandRegexProvider regexProvider;

    /**
     * Map of invokables for input sanitation
     */
    private final CommandSanityChecker sanityChecker;

    public RegexSmartParser(CommandRegexProvider regexProvider, CommandSanityChecker sanityChecker) {

        this.regexProvider = regexProvider;

        this.sanityChecker = sanityChecker;

    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalArgumentException if a non-exist flag is found post-parse
     */
    @Override
    public Map<String, String[]> mapUserInput(String toParse) {

        /*
            HashMap containing results
         */
        HashMap<String, String[]> results = new HashMap<>();

        /*
            Entry representing the current entry. Initialized as default entry
         */
        AbstractMap.SimpleEntry<String, ArrayList<String>> workingEntry =
                new AbstractMap.SimpleEntry<>(HasFlags.DEFAULT_FLAG, new ArrayList<String>());


        Matcher exploder = regexProvider.getExplosionPattern().matcher(toParse);

        /*
                A simple interlock to prevent exclusion of command-similar strings (think negative integers and commands) inside ignore groups.
             */
        boolean foundHandle = false;

        while (exploder.find()) {

            String element = regexProvider.postProcessString(exploder.group());

            /*
                If the element is the handle, throw it out
             */
            if (regexProvider.isCommand(element) && !foundHandle) {
                foundHandle = true;
            } else if (regexProvider.isFlag(element)) {

                /*
                    We add the current working element to the map
                 */
                String flagName = workingEntry.getKey();
                String[] compiledArgs = workingEntry.getValue().toArray(new String[sanityChecker.getArity(flagName)]);

                results.put(flagName, compiledArgs);

                String newFlagName = regexProvider.getFlag(element);

                /*
                    Create the new workingEntry
                 */
                workingEntry = new AbstractMap.SimpleEntry<String, ArrayList<String>>(newFlagName, new ArrayList<String>());

            } else {

                workingEntry.getValue().add(element);

            }

        }

        /*
            Add the final working entry
         */
        String flagName = workingEntry.getKey();
        String[] compiledArgs = workingEntry.getValue().toArray(new String[sanityChecker.getArity(flagName)]);
        results.put(flagName, compiledArgs);

        /*
            Perform sanitation checks on compiled commands
         */
        for (Map.Entry<String, String[]> entryCheck : results.entrySet()) {
            if (!sanityChecker.flagIsSane(entryCheck.getKey(), entryCheck.getValue().length))
                throw new IllegalArgumentException(entryCheck.getKey() + " failed sanity checks");
        }

        return results;
    }

}

