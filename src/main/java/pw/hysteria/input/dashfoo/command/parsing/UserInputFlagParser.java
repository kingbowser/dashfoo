

/*
 * Copyright 2014. Hysteria Working Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pw.hysteria.input.dashfoo.command.parsing;

import java.util.Map;

/**
 * Defines a contract that parses user input for use in command dispatching
 * Creator: KingBowser
 * Date: 5/26/13
 * Time: 10:40 PM
 * Refer to LICENSE for licensing information
 */
public interface UserInputFlagParser {

    /**
     * Should identify the parameters that go to the default invokable (IE leftovers)
     */
    public static final String DEFAULT_PARAM = "@default";

    /**
     * Crete the flag-arg user input map
     *
     * @param toParse flags input
     * @return flag:args[] input map
     */
    public Map<String, String[]> mapUserInput(String toParse);

}
